from flask import Blueprint, render_template, request, redirect
# from MainApplication.Aggregator import CombineResults as Searches
import SearchEngines.Bing.BingSearch as Bing
import MainApplication.Aggregator.external_links as Externals
from MainApplication.exchange_dict import exchange_dict
import MainApplication.QueryAnalyzer as QueryAnalyzer
import SearchEngines.WikiMedia.InstantExtracts as InstantExtracts
import SearchEngines.Infinity.Infinity as Infinity
import SearchEngines.Infinity.news as InfinityNews
import random


ads = [
    # Sponsored Ads
    ['Simple Login', 'Open source email alias solution to protect your mailbox', 'https://simplelogin.io', ''],
    ['myFrontpage', 'Create your own personal New-Tab page for your Browser without any code for free!',
     'https://myfront.page', ''],
    ['Mailfence',
     'Your privacy is being violated daily, make a stand, and subscribe to an email provider that actually cares about your privacy.',
     'https://mailfence.com?src=infini', ''],
    ['Mailfence',
     'Getting your privacy back can be hard, but you can start by subscribing Mailfence. Get your account today!',
     'https://mailfence.com?src=infini', ''],
    ['Snap Search', 'A Privacy-First Browser for Android with Super Incognito Mode', 'https://snapsearch.online', '']

]

resultsAPI = Blueprint('resultsAPI', __name__)


def display_homepage():
    messages = ['Search with privacy and efficiency.']
    message = messages[random.randint(0, len(messages) - 1)]
    return render_template('pages/home.html', message=message)


@resultsAPI.route('/', methods=['GET', 'POST'])
def render_static():
    if request.method == 'POST':
        return render_results()
    else:
        try:
            if request.args.get('q') is not None:
                query = request.args.get('q')
                return redirect('/results?q=' + query)

        except Exception:
            return display_homepage()

        return display_homepage()


def get_results(query, page_number=1):
    instant_extracts = []
    if page_number == 1:
        instant_extracts = InstantExtracts.get_answers(query)

    try:
        results = Bing.get_all(query, offset=page_number)
        print(results)

    except Exception as e:
        print(e)

    infinity_results = []
    if page_number == 1 and len(results) == 0:
        try:
            infinity_results = Infinity.get_results(query, 1)
        except Exception as e:
            infinity_results = []

    if len(instant_extracts) > 0:
        for link in results:
            try:
                if link['url'] == instant_extracts[0]['page_url']:
                    results.remove(link)
            except Exception:
                continue

    ads_length_index = len(ads) - 1
    top_ad = ads[random.randint(0, ads_length_index)]

    stock_searched = False
    stock = ''

    symbol = ''
    url = ''

    definition = []

    stock_news = False
    crypto_news = False

    calculation = []

    html_space = []

    try:
        integrations = QueryAnalyzer.analyze_query_v2(query)
    except Exception:
        integrations = '', ''


    return render_template('results/fetch.html', query=query, stock=stock, stock_searched=stock_searched,
                           symbol=symbol, url=url, stock_news=stock_news, crypto_news=crypto_news,
                           instant_extracts=instant_extracts, bing_results=results,
                           definition=definition,
                           top_ad=top_ad, current='web',
                           calculator=calculation, page_number=page_number, html_space=html_space,
                           limit_results=False, infinity_results=infinity_results)


@resultsAPI.route('/results2', methods=['GET', 'POST'])
def render_results():
    if request.method == 'POST':
        try:
            form_results = dict(request.form)
            query = form_results['Search']

            page_number = 1
            if 'page_number' in form_results:
                try:
                    page_number = int(form_results['page_number'])
                except Exception:
                    page_number = 1

            return get_results(query, page_number)

        except Exception:
            return redirect('/')

    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_results(query, page_number)

    except Exception:
        return redirect('/')

    else:
        return redirect('/')


def prepare_results(query, page_number):
    stock_searched = False
    stock = ''

    symbol = ''
    url = ''

    definition = []

    stock_news = False
    crypto_news = False

    calculation = []

    html_space = []

    html_editor = []

    try:
        integrations = QueryAnalyzer.analyze_query_v2(query)
    except Exception:
        integrations = '', ''

    if integrations[0] == 'calculator':
        calculation = [[]]

    elif integrations[0] == 'stock':
        stock_searched = True
        stock = integrations[1].upper()
        try:
            exchange = exchange_dict[stock]

        except Exception:
            exchange = 'NASDAQ'

        symbol = exchange.upper() + ':' + stock
        url = 'https://www.tradingview.com/symbols/' + exchange + '-' + stock + '/'

    # elif integrations[0] == 'crypto':
    #     crypto_news = True

    elif integrations[0] == 'stock news':
        stock_news = True

    elif integrations[0] == 'html space':
        html_space = ['&nbsp;']

    elif integrations[0] == 'html_editor':
        html_editor = [[]]

    external_links = Externals.get_external_links(query)

    ads_length_index = len(ads) - 1
    top_ad = ads[random.randint(0, ads_length_index)]

    return render_template('results/layout.html', query=query, url='/results2', external_results=external_links,
                           current='web', page_number=page_number,
                           stock=stock, stock_searched=stock_searched,
                           symbol=symbol, tv_url=url, stock_news=stock_news, crypto_news=crypto_news,
                           html_space=html_space, calculator=calculation, html_editor=html_editor, top_ad=top_ad
                           )


@resultsAPI.route('/results', methods=['GET', 'POST'])
def render_results2():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

            page_number = 1
            if 'page_number' in form_results:
                try:
                    page_number = int(form_results['page_number'])
                except Exception:
                    page_number = 1

            return prepare_results(query, page_number)

        except Exception as e:
            return redirect('/')

    # If GET request
    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))
        if request.args.get('q') is None:
            return redirect('/')
        query = request.args.get('q')

        return prepare_results(query, page_number)
    except Exception:
        return redirect('/')


# Images ------------------

def get_image_results(query, page_number=1):
    results = []

    return render_template('results/fetch.html', query=query, image_results=results,
                           current='images', page_number=page_number, )


@resultsAPI.route('/results/images2', methods=['GET', 'POST'])
def render_image_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

            page_number = 1
            if 'page_number' in form_results:

                try:
                    page_number = int(form_results['page_number'])
                except Exception:
                    page_number = 1

        except Exception:
            return redirect('/')

        return get_image_results(query, page_number=page_number)

    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))

        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_image_results(query, page_number=page_number)

    except Exception:
        return redirect('/')

    return redirect('/')


@resultsAPI.route('/results/images', methods=['GET', 'POST'])
def render_image_results2():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

            page_number = 1
            if 'page_number' in form_results:
                try:
                    page_number = int(form_results['page_number'])
                except Exception:
                    page_number = 1

        except Exception:
            return redirect('/')

        external_links = Externals.get_image_links(query)
        return render_template('results/layout.html', query=query, url='/results/images2', page_number=page_number,
                               external_results=external_links, current='images')

    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))
        if request.args.get('q') is not None:
            query = request.args.get('q')

            external_links = Externals.get_image_links(query)
            return render_template('results/layout.html', query=query, url='/results/images2', page_number=page_number,
                                   external_results=external_links, current='images')

            # return render_template('pages/t1.html', query=query, page_number=page_number, url='/results/images2')

    except Exception:
        return redirect('/')

    else:
        return redirect('/')


# Video  Results ------------------------
def get_video_results(query):
    # results = Invidious.get_video_results(query)
    results = []

    return render_template('results/fetch.html', query=query, video_results=results,
                           current='videos')

    # return render_template('results/results.html', query=query, video_results=results,
    #                     current='videos')


@resultsAPI.route('/results/videos2', methods=['GET', 'POST'])
def render_video_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

            return get_video_results(query)

        except Exception as e:
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_video_results(query)

    except Exception as e:
        return redirect('/')

    return redirect('/')


@resultsAPI.route('/results/videos', methods=['GET', 'POST'])
def render_video_results2():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']
            external_links = Externals.get_video_links(query)

            return render_template('results/layout.html', query=query, url='/results/videos2',
                                   external_results=external_links, current='videos')

            # return render_template('pages/t1.html', query=query, url='/results/videos')

        except Exception as e:
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            external_links = Externals.get_video_links(query)

            return render_template('results/layout.html', query=query, url='/results/videos2',
                                   external_results=external_links, current='videos')

            # return render_template('pages/t1.html', query=query, url='/results/videos2')

    except Exception as e:
        return redirect('/')

    return redirect('/')


# News  Results ------------------------
def get_news_results(query):
    try:
        results = InfinityNews.get_results(query, 5)
    except Exception:
        results = []

    return render_template('results/fetch.html', query=query, news_results=results,
                           current='news')

@resultsAPI.route('/results/news2', methods=['GET', 'POST'])
def render_news_results():
    if request.method == 'POST':
        try:
            form_results = dict(request.form)
            query = form_results['Search']
            return get_news_results(query)

        except Exception as e:
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_news_results(query)

    except Exception as e:
        return redirect('/')

    return redirect('/')


@resultsAPI.route('/results/news', methods=['GET', 'POST'])
def render_news_results2():
    ads_length_index = len(ads) - 1
    top_ad = ads[random.randint(0, ads_length_index)]

    if request.method == 'POST':
        try:
            form_results = dict(request.form)
            query = form_results['Search']
            external_links = Externals.get_news_links(query)

            return render_template('results/layout.html', query=query, url='/results/news2',
                                   external_results=external_links, current='news', top_ad=top_ad)

        except Exception as e:
            print(e)
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            external_links = Externals.get_news_links(query)
            return render_template('results/layout.html', query=query, url='/results/news2',
                                   external_results=external_links, current='news', top_ad=top_ad)

    except Exception as e:
        return redirect('/')

    return redirect('/')


# Infinity  Results ------------------------
def get_infinity_results(query):
    # results = Infinity.get_wiki_results(query)
    results = Infinity.search_pymongo_index(query)

    # print(results)
    # external_links = Externals.get_external_links(query)
    instant_extracts = InstantExtracts.get_answers(query)

    return render_template('results/fetch.html', query=query, infinity_results=results,
                           current='infinity', instant_extracts=instant_extracts)

    # return render_template('results/results.html', query=query, infinity_results=results,
    #                        external_results=external_links, current='infinity', instant_extracts=instant_extracts)


@resultsAPI.route('/results/infinity2', methods=['GET', 'POST'])
def render_infinity_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

            return get_infinity_results(query)

        except Exception as e:
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_infinity_results(query)

    except Exception as e:
        return redirect('/')

    return redirect('/')


@resultsAPI.route('/results/infinity', methods=['GET', 'POST'])
def render_infinity_results2():
    ads_length_index = len(ads) - 1
    top_ad = ads[random.randint(0, ads_length_index)]

    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']
            external_links = Externals.get_external_links(query)

            return render_template('results/layout2.html', query=query, url='/results/infinity2',
                                   external_results=external_links, current='infinity', top_ad=top_ad)

        except Exception as e:
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            external_links = Externals.get_external_links(query)
            return render_template('results/layout.html', query=query, url='/results/infinity2',
                                   external_results=external_links, current='infinity', top_ad=top_ad)
            # return render_template('pages/t1.html', query=query, url='/results/infinity2')

    except Exception as e:
        return redirect('/')

    return redirect('/')

# Shopping ------------------
#
# def get_shopping_results(query, page_number=1):
#     words = query
#     words = str(words).split()
#     if len(words) == 0:
#         return render_template('results/fetch.html', query=query, shopping_results=[],
#                                current='shopping', page_number=page_number)
#     try:
#         results = BingShopping.get_shopping_results(query, page_number)
#     except Exception:
#         results = []
#
#     return render_template('results/fetch.html', query=query, shopping_results=results,
#                            current='shopping', page_number=page_number)
#
#     # return render_template('results/results.html', query=query, shopping_results=results,
#     #                         current='shopping', page_number=page_number, limit_results=limit_results)
#
#
# @resultsAPI.route('/results/shopping2',  methods=['GET', 'POST'])
# def render_shopping_results():
#     if request.method == 'POST':
#         try:  # In case someone tried to change the value of the form name
#             form_results = dict(request.form)
#             query = form_results['Search']
#
#             page_number = 1
#             if 'page_number' in form_results:
#
#                 try:
#                     page_number = int(form_results['page_number'])
#                 except Exception:
#                     page_number = 1
#
#         except Exception:
#             return redirect('/')
#
#         return get_shopping_results(query, page_number=page_number)
#
#     try:
#         page_number = 1
#         if request.args.get('page_number'):
#             page_number = int(request.args.get('page_number'))
#
#         if request.args.get('q') is not None:
#             query = request.args.get('q')
#             return get_shopping_results(query, page_number=page_number)
#
#     except Exception:
#         return redirect('/')
#
#     return redirect('/')
#
# @resultsAPI.route('/results/shopping',  methods=['GET', 'POST'])
# def render_shopping_results2():
#     if request.method == 'POST':
#         try:  # In case someone tried to change the value of the form name
#             form_results = dict(request.form)
#             query = form_results['Search']
#
#             page_number = 1
#             if 'page_number' in form_results:
#                 try:
#                     page_number = int(form_results['page_number'])
#                 except Exception:
#                     page_number = 1
#
#         except Exception:
#             return redirect('/')
#
#         external_links = Externals.get_shopping_links(query)
#         return render_template('results/layout.html', query=query, url='/results/shopping2', page_number=page_number,
#                                external_results=external_links, current='shopping')
#
#     try:
#         page_number = 1
#         if request.args.get('page_number'):
#             page_number = int(request.args.get('page_number'))
#         if request.args.get('q') is not None:
#             query = request.args.get('q')
#
#             external_links = Externals.get_shopping_links(query)
#             return render_template('results/layout.html', query=query, url='/results/shopping2', page_number=page_number, external_results=external_links, current='shopping')
#
#             # return render_template('pages/t1.html', query=query, page_number=page_number, url='/results/shopping2')
#
#     except Exception:
#         return redirect('/')
#
#     else:
#         return redirect('/')
#
#
