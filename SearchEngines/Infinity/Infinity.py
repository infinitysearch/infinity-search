from urllib.parse import urlparse
from accounts import mongodb_endpoint
import pymongo


def format_empty_title(url):
    parsed = urlparse(url)
    path = parsed[2]

    if path.endswith('/'):
        path = path[:-1]

    path = path.rsplit('/', 1)[-1]

    title = path.replace('-', ' ')
    title = title.replace('_', ' ')
    title = title.replace('%20', ' ')

    if title.endswith('.html'):
        title = title[:-5]

    if title.endswith('.htm'):
        title = title[:-4]

    return title


def search_pymongo_index(query):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinitySearchEngine']
    col = db['index1']
    results = col.aggregate([
        {
            "$search": {
                "text": {
                    "query": str(query),
                    "path": "link"
                }
            }
        },
        {
            "$limit": 10
        },
        {
            "$project": {
                "_id": 0,
                "link": 1,
                "title": 1
                # "time": 1
            }
        }
    ])

    formatted_results = []
    for res in results:

        if res['title'] == '':
            res['title'] = format_empty_title(res['link'])

        formatted_results.append([res['link'], res['title']])

    return formatted_results


from func_timeout import func_timeout, FunctionTimedOut, func_set_timeout

def get_results(query, timeout_time):
    try:
        results = func_timeout(timeout_time, search_pymongo_index, args=(query,))
    except FunctionTimedOut:
        results = []
    except Exception as e:
        results = []

    return results
